from hotel.models.guest import Guest
from uuid import uuid4


def register_new_guest(guest_data):
    try:
        print("data========>", guest_data)
        guest_name = guest_data['Guest Name']
        guest_email = guest_data['Email']
        guest_country = guest_data['Guest Country']
        guest_state = guest_data['Guest State']
        guest_district = guest_data['Guest District']
        guest_city = guest_data['Guest City']
        guest_street = guest_data['Guest Street']
        guest_pincode = guest_data['Guest Pin Code']
        guest_uid = uuid4()
        guest_id_proof = guest_data['Guest Id Proof']
        guest_password = guest_data['password']
        guest_object = Guest(guest_name, guest_email, guest_country, guest_state,
                             guest_district, guest_city, guest_street, guest_pincode,
                             guest_uid, guest_id_proof, guest_password
                             )
        guest_object.save_to_db()
    except Exception as e:
        print("error===========>", e)
