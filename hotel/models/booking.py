from hotel.models import db
import datetime
from sqlalchemy import DateTime
from hotel.models.base import Base

from sqlalchemy.orm import backref


class Booking(db.Model, Base):
    __tablename__ = "booking"
    id = db.Column(db.Integer, primary_key=True)
    guestId = db.Column(db.Integer, db.ForeignKey("guest.id", ondelete='CASCADE'), nullable=False)
    roomId = db.Column(db.Integer, db.ForeignKey("room.id", ondelete='CASCADE'), nullable=False)
    amenitiesId = db.Column(db.Integer, db.ForeignKey("amenities.id", ondelete='CASCADE'), nullable=False)
    bookingStatus = db.Column(db.Integer)
    bookingDate = db.Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    checkIn = db.Column(DateTime, nullable=False)
    checkOut = db.Column(DateTime, nullable=False)

    # relationship
    guest = db.relationship("Guest", backref=backref("guest"))
    room = db.relationship("Room", backref=backref("room"))
    amenities = db.relationship("Amenities", backref=backref("amenities"))
    payment = db.relationship("Payment", backref=backref("Booking", uselist=False))

    def __init__(self, guest_id, room_id, amenities_id, booking_status, booking_date, check_in, check_out):
        self.guestId = guest_id
        self.roomId = room_id
        self.amenitiesId = amenities_id
        self.bookingStatus = booking_status
        self.bookingDate = booking_date
        self.checkIn = check_in
        self.checkOut = check_out
