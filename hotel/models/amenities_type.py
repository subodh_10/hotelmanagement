from hotel.models import db
from hotel.models.base import Base


# from amenities import Amenities


# create model class for amenities
class AmenitiesType(db.Model, Base):
    __tablename__ = "amenities_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    description = db.Column(db.String(100))

    # amenities = db.relationship('Amenities', backref='amenities_type')

    def __init__(self, name, description, cost):
        self.name = name
        self.description = description
        self.cost = cost
