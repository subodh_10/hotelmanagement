from hotel.models import db
from hotel.models.base import Base


class Room(db.Model, Base):
    __tablename__ = "room"
    id = db.Column(db.Integer, primary_key=True)
    roomNumber = db.Column(db.Integer)
    roomType = db.Column(db.Integer, db.ForeignKey('room_type.id'), nullable=False)
    status = db.Column(db.String(100))

    def __init__(self, room_number, room_type, status):
        self.roomNumber = room_number
        self.roomType = room_type
        self.status = status
