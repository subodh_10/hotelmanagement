from hotel.models import db


class Base(object):
    """base class for all model's classes
    which is inherited by all model's classes"""
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
