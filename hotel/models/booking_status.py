from hotel.models import db
from hotel.models.base import Base
from sqlalchemy.orm import backref


class BookingStatus(db.Model, Base):
    __tablename__ = "booking_status"
    id = db.Column(db.Integer, primary_key=True)
    bookingId = db.Column(db.Integer, db.ForeignKey("booking.id", ondelete='CASCADE'), nullable=False)
    statusType = db.Column(db.String(100), nullable=False)
    statusDesc = db.Column(db.String(127), nullable=False)

    # relationship
    room = db.relationship("Booking", backref=backref("booking", uselist=False))

    def __init__(self, booking_id, status_type, status_desc):
        self.bookingId = booking_id
        self.statusType = status_type
        self.statusDesc = status_desc
