from hotel.models import db
from hotel.models.amenities_type import AmenitiesType
from hotel.models.base import Base


# create model class for amenities
class Amenities(db.Model, Base):
    __tablename__ = "amenities"
    id = db.Column(db.Integer, primary_key=True)
    amenitiesTypeId = db.Column(db.Integer, db.ForeignKey('amenities_type.id'), nullable=False)
    description = db.Column(db.String(100))
    cost = db.Column(db.Float)

    # relationship
    amenities_type = db.relationship('AmenitiesType', backref='amenities')

    def __init__(self, amenities_type_id, description, cost):
        self.type = amenities_type_id
        self.description = description
        self.cost = cost
