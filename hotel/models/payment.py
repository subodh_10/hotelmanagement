from hotel.models import db
import datetime
from sqlalchemy import DateTime
from hotel.models.base import Base


class Payment(db.Model, Base):
    __tablename__ = "payment"
    id = db.Column(db.Integer, primary_key=True)
    bookingId = db.Column(db.Integer, db.ForeignKey("booking.id", ondelete='CASCADE'), nullable=False)
    amount = db.Column(db.Float)
    paymentType = db.Column(db.String(100))
    paymentDate = db.Column(DateTime, default=datetime.datetime.utcnow)
    status = db.Column(db.String(100))

    def __init__(self, booking_id, amount, payment_type, payment_date, status):
        self.bookingId = booking_id
        self.amount = amount
        self.paymentType = payment_type
        self.paymentDate = payment_date
        self.status = status
