from hotel.models import db
from hotel.models.base import Base

# from base import Base


class Guest(db.Model, Base):
    id = db.Column(db.Integer, primary_key=True)
    guestName = db.Column(db.String(80), nullable=False)
    guestEmail = db.Column(db.String(80), unique=True)
    guestCountry = db.Column(db.String(100), nullable=False)
    guestState = db.Column(db.String(100))
    guestDistrict = db.Column(db.String(100))
    guestCity = db.Column(db.String(100))
    guestStreet = db.Column(db.String(100))
    guestPinCode = db.Column(db.String(100))
    guestUId = db.Column(db.String(100), unique=True, nullable=False)
    guestIdProof = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(500), nullable=False)

    # create constructor
    def __init__(self, guest_name, guest_email, guest_country, guest_state,
                 guest_district, guest_city, guest_street, guest_pincode,
                 guest_uid, guest_id_proof, guest_password):
        self.guestName = guest_name
        self.guestEmail = guest_email
        self.guestCountry = guest_country
        self.guestState = guest_state
        self.guestDistrict = guest_district
        self.guestCity = guest_city
        self.guestStreet = guest_street
        self.guestPinCode = guest_pincode
        self.guestUId = guest_uid
        self.guestIdProof = guest_id_proof
        self.password = guest_password
