from hotel.models import db
from hotel.models.base import Base


# define admin class with attribute
class Admin(db.Model, Base):
    __tablename__ = "admin"
    adminId = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    registerDate = db.Column(db.DateTime, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    uniqueId = db.Column(db.String(90), unique=True, nullable=False)  # unique id, useful for security

    def __init__(self, name, email_id, register_date, password, unique_id):
        self.name = name
        self.email = email_id
        self.registerDate = register_date
        self.password = password
        self.uniqueId = unique_id
