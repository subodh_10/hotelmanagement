from hotel.models import db
from hotel.models.base import Base


class RoomType(db.Model, Base):
    __tablename__ = "room_type"
    id = db.Column(db.Integer, primary_key=True)
    roomType = db.Column(db.String(100))
    capacity = db.Column(db.Integer)
    description = db.Column(db.String(600))
    cost = db.Column(db.Integer)
    roomImg = db.Column(db.String(100))

    # one to many relationship
    # rooms = db.relationship('Room', backref='room_type')

    def __init__(self, room_type, capacity, description, room_image):
        self.roomType = room_type
        self.capacity = capacity
        self.description = description
        self.roomImg = room_image
