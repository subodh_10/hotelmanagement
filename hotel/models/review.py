from hotel.models import db
import datetime
from sqlalchemy import DateTime
from hotel.models.base import Base


class Review(db.Model, Base):
    __tablename__ = "review"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    guestId = db.Column(db.Integer, db.ForeignKey("guest.id", ondelete='CASCADE'), nullable=False)
    star = db.Column(db.Integer)
    text = db.Column(db.String(500))
    reviewDate = db.Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, guest_id, title, star, text, review_date):
        self.guestId = guest_id
        self.title = title
        self.star = star
        self.text = text
        self.reviewDate = review_date
