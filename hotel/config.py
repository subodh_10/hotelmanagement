class Config(object):
    DEBUG = True


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql://sabu:Subodh_10@localhost:3306/hotel'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    PORT = 5001


config_settings = {
    "development": DevelopmentConfig
}
