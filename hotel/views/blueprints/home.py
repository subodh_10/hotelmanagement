from flask import Blueprint, render_template

home = Blueprint("home", __name__)


@home.route('/', methods=['GET'])
def index():
    return render_template("home.html", message="Hello Flask!");


@home.route('/signup', methods=['GET'])
def guest_sign_in():
    return render_template("guest/signup.html", message="Hello Flask!");
