from flask import Blueprint, render_template, request
import uuid
from hotel.utils.database.guest import register_new_guest

guest = Blueprint("guest", __name__)


@guest.route("/signUp", methods=['GET', 'POST'])
def signup():
    if request.method == "GET":
        return render_template("guest/signup.html")

    elif request.method == "POST":
        try:
            # convert into dictionary
            guest_data = request.form.to_dict()
            register_new_guest(guest_data)
            return render_template('guest/sign_in.html')

        except Exception as e:
            print(e)
            return render_template("guest/signup.html")


@guest.route('/login', methods=['POST'])
def login():
    # creates dictionary of form data
    auth = request.form
    print("data -------------------->", auth.get('email'))
    return render_template('guest/booking.html')
    #
    # if not auth or not auth.get('email') or not auth.get('password'):
    #     # returns 401 if any email or / and password is missing
    #     return make_response(
    #         'Could not verify',
    #         401,
    #         {'WWW-Authenticate': 'Basic realm ="Login required !!"'}
    #     )
    #
    # user = User.query \
    #     .filter_by(email=auth.get('email')) \
    #     .first()
    #
    # if not user:
    #     # returns 401 if user does not exist
    #     return make_response(
    #         'Could not verify',
    #         401,
    #         {'WWW-Authenticate': 'Basic realm ="User does not exist !!"'}
    #     )
    #
    # if check_password_hash(user.password, auth.get('password')):
    #     # generates the JWT Token
    #     token = jwt.encode({
    #         'public_id': user.public_id,
    #         'exp': datetime.utcnow() + timedelta(minutes=30)
    #     }, app.config['SECRET_KEY'])
    #
    #     return make_response(jsonify({'token': token.decode('UTF-8')}), 201)
    # # returns 403 if password is wrong
    # return make_response(
    #     'Could not verify',
    #     403,
    #     {'WWW-Authenticate': 'Basic realm ="Wrong Password !!"'}
    # )
#

# @app.route('/signup', methods=['POST'])
# def signup():
#     # creates a dictionary of the form data
#     data = request.form
#
#     # gets name, email and password
#     name, email = data.get('name'), data.get('email')
#     password = data.get('password')
#
#     # checking for existing user
#     user = User.query \
#         .filter_by(email=email) \
#         .first()
#     if not user:
#         # database ORM object
#         user = User(
#             public_id=str(uuid.uuid4()),
#             name=name,
#             email=email,
#             password=generate_password_hash(password)
#         )
#         # insert user
#         db.session.add(user)
#         db.session.commit()
#
#         return make_response('Successfully registered.', 201)
#     else:
#         # returns 202 if user already exists
#         return make_response('User already exists. Please Log in.', 202)
#
