from flask import Flask
from hotel.config import config_settings
from flask_restful import Api
from hotel.models import db

# import all model's classes
from hotel.models.admin import Admin
from hotel.models.amenities import Amenities
from hotel.models.amenities_type import AmenitiesType
from hotel.models.room_type import RoomType
from hotel.models.room import Room
from hotel.models.guest import Guest
from hotel.models.booking import Booking
from hotel.models.booking_status import BookingStatus
from hotel.models.payment import Payment
from hotel.models.review import Review

from flask_migrate import Migrate

migrate = Migrate()


def create_app():
    app = Flask(__name__, template_folder='templates', static_folder='static')
    app.config.from_object(config_settings['development'])
    app.config['PORT'] = 5001
    app.config['running_host'] = 'localhost'

    api = Api(app)
    # register resource end point
    # api.add_resource(HelloWorld, '/')

    # import all resource classes below
    from hotel.views.blueprints.home import home
    from hotel.views.blueprints.guest import guest
    from hotel.views.blueprints.admin import admin_bp

    # register blueprint here
    app.register_blueprint(home)
    app.register_blueprint(guest, url_prefix='/guest')
    app.register_blueprint(admin_bp, url_prefix="/admin")

    db.init_app(app)
    migrate.init_app(app, db)
    return app
