from hotel import create_app
import os

app = create_app()

if __name__ == '__main__':
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    os.environ["OAUTHLIB_RELAX_TOKEN_SCOPE"] = '1'
    app.run(host=app.config['running_host'], port=5001,threaded=True)
